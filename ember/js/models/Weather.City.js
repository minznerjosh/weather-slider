Weather.City = Ember.Object.extend({
	conditionCode: null,
	temp: null,
	condition: null,
	city: null,
	region: null,
	location: function() {
		return this.get('city') + ', ' + this.get('region');
	}.property('city', 'region'),
	humidity: null,
	wind: null,
	forecast: []
});

Weather.City.reopenClass({
	fetchZIPs: function(zips) {
		require('models/Weather.ForecastItem');
		
		var self = this;
		var results = [];
		
		zips.forEach(function(zip) {
			var city = self.create();
			
			$.queryYQL('select * from xml where url="http://xml.weather.yahoo.com/forecastrss/' + zip + '_f.xml"', function(data) {
				var data = data.query.results.rss.channel;
				
				city.set('conditionCode', data.item.condition.code);
				city.set('temp', data.item.condition.temp);
				city.set('condition', data.item.condition.text);
				city.set('city', data.location.city);
				city.set('region', data.location.region);
				city.set('humidity', data.atmosphere.humidity);
				city.set('wind', data.wind.direction + '&deg; at ' + data.wind.speed);
				city.set('forecast', Weather.ForecastItem.createFromYahoo(data.item.forecast));
			});
			
			results.pushObject(city);
		});
		
		return results;
	},
	fetchByNames: function(names) {
		require('models/Weather.ForecastItem');
		
		var self = this;
		var results = [];
		
		names.forEach(function(name) {
			var city = self.create();
			
			$.queryYQL('use \'http://github.com/yql/yql-tables/raw/master/weather/weather.bylocation.xml\' as we;select * from we where location="' + name + '" and unit=\'f\'', function(data) {
				var weatherData = data.query.results.weather.rss.channel;
				
				console.log(weatherData);
				
				city.set('conditionCode', weatherData.item.condition.code);
				city.set('temp', weatherData.item.condition.temp);
				city.set('condition', weatherData.item.condition.text);
				city.set('city', weatherData.location.city);
				city.set('region', weatherData.location.region);
				city.set('humidity', weatherData.atmosphere.humidity);
				city.set('wind', weatherData.wind.direction + '&deg; at ' + weatherData.wind.speed);
				city.set('forecast', Weather.ForecastItem.createFromYahoo(weatherData.item.forecast));
			});
			
			results.pushObject(city);
		});
		
		return results;
	}
});