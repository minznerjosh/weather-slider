Weather.ForecastItem = Ember.Object.extend({
	date: function(key, value) {
		if (arguments.length === 1) {
			return this.get('_date');
		} else {
			this.set('_date', new Date(value));
			return this.get('_date');
		}
	}.property('_date'),
	conditionCode: null,
	high: null,
	low: null,
	text: null
});

Weather.ForecastItem.reopenClass({
	createFromYahoo: function(forecast) {
		var self = this;
		var results = [];
		
		forecast.forEach(function(item) {
			var forecastItem = self.create();
			
			forecastItem.set('date', item.date);
			forecastItem.set('conditionCode', item.code);
			forecastItem.set('high', item.high);
			forecastItem.set('low', item.low);
			forecastItem.set('text', item.text);
			
			results.pushObject(forecastItem);
		});
		
		return results;
	}
});