Weather.SliderSlide = Ember.View.extend({
	templateName: 'slide',
	classNameBindings: ['positionOnScreen', 'bgClass'],
	classNames: ['slide'],
	
	bgClass: function() {
		return 'code' + this.get('content.conditionCode');
	}.property('content.conditionCode'),
	
	isFirst: function() {
		if (this.get('parentView.firstView') == this) return true;
		return false;
	}.property('parentView.firstView'),
	
	isLast: function() {
		if (this.get('parentView.lastView') == this) return true;
		return false;
	}.property('parentView.lastView'),
	
	positionOnScreen: function() {
		var siblingsAndMe = this.get('parentView.childViews');
		
		if (this.get('parentView.visibleView') == this) {
			return '';
		} else if (siblingsAndMe.indexOf(this) < siblingsAndMe.indexOf(this.get('parentView.visibleView'))) {
			return 'hidden-left';
		} else {
			return 'hidden-right';
		}
	}.property('parentView.visibleView'),
});