Weather.ForecastTile = Ember.View.extend({
	tagName: 'span',
	classNameBindings: ['bgClass'],
	classNames: ['forecast-item'],
	
	bgClass: function() {
		return 'code' + this.get('context.conditionCode');
	}.property('content.conditionCode'),
	
	dateText: function() {
		var today = new Date();
		var tomorrow = new Date();
		tomorrow.setDate(today.getDate() + 1);
		
		var date = this.get('context.date');
		
		if (date.getDate() === today.getDate()) {
			return 'Today';
		} else if (date.getDate() === tomorrow.getDate()) {
			return 'Tomorrow';
		} else {
			return date.format('dddd');
		}
	}.property('context.date')
});