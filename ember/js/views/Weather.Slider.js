require('views/Weather.SliderSlide');

Weather.Slider = Ember.View.extend({
	template: Ember.Handlebars.compile('{{view view.slider viewName="_slider"}}{{view view.controls}}{{view view.tabs}}'),
	
	slider: Ember.CollectionView.extend({
		itemViewClass: Weather.SliderSlide,
		contentBinding: 'parentView.content',
		visibleViewBinding: Ember.Binding.oneWay('firstView'),
		
		nextView: function() {
			var visibleViewIndex = this.get('childViews').indexOf(this.get('visibleView'));
			var nextView = this.get('childViews').objectAt(visibleViewIndex + 1);
			
			if (nextView) return nextView;
			return this.get('firstView');
		}.property('childViews.@each', 'visibleView'),
		
		previousView: function() {
			var visibleViewIndex = this.get('childViews').indexOf(this.get('visibleView'));
			var previousView = this.get('childViews').objectAt(visibleViewIndex - 1);
			
			if (previousView) return previousView;
			return this.get('lastView');
		}.property('childViews.@each', 'visibleView'),
		
		firstView: function() {
			return this.get('childViews.firstObject');
		}.property('childViews.@each'),
		
		lastView: function() {
			return this.get('childViews.lastObject');
		}.property('childViews.@each'),
		
		moveToSlideWithContent: function(content) {
			var self = this;
			
			this.get('childViews').forEach(function(view) {
				if (view.get('content') == content) self.set('visibleView', view);
			});
		}
	}),
	
	controls: Ember.View.extend({
		templateName: 'controls'
	}),
	
	tabs: Ember.CollectionView.extend({
		contentBinding: 'parentView.content',
		classNames: ['tabs'],
		itemViewClass: Ember.View.extend({
			tagName: 'span',
			classNames: 'tab',
			classNameBindings: ['active'],
			template: Ember.Handlebars.compile('{{view.content.city}}'),
			
			active: function() {
				if (this.get('content') == this.get('parentView.parentView._slider.visibleView.content')) return true;
				return false;
			}.property('content', 'parentView.parentView._slider.visibleView.content'),
			
			click: function() {
				this.get('parentView.parentView._slider').moveToSlideWithContent(this.get('content'));
			}
		})
	}),
	
	next: function() {
		this.set('_slider.visibleView', this.get('_slider.nextView'));
	},
	previous: function() {
		this.set('_slider.visibleView', this.get('_slider.previousView'));
	}
});