Weather.Router.map(function() {
	this.route('index', { path: '/' });
});

Weather.IndexRoute = Ember.Route.extend({
	model: function() {
		require('models/Weather.City');
		return Weather.City.fetchByNames(['newyorkcity,ny', 'berlin,germany', 'hongkong,china', 'dubai', 'toronto,canada', 'london,england']);
	},
	setupController: function(controller, model) {
		this.controllerFor('application').set('content', model);
	}
});